|名称|Stars|简介|备注|
|---|---|---|---|
|[sadmann7/skateshop](https://github.com/sadmann7/skateshop)|![GitHub Repo stars](https://badgen.net/github/stars/sadmann7/skateshop)|An open source e-commerce skateshop build with everything new in Next.js 13.|-|
|[sveltejs/svelte](https://github.com/sveltejs/svelte)|![GitHub Repo stars](https://badgen.net/github/stars/sveltejs/svelte)|Cybernetically enhanced web apps|-|
|[Stability-AI/generative-models](https://github.com/Stability-AI/generative-models)|![GitHub Repo stars](https://badgen.net/github/stars/Stability-AI/generative-models)|Generative Models by Stability AI|-|
|[CASIA-IVA-Lab/FastSAM](https://github.com/CASIA-IVA-Lab/FastSAM)|![GitHub Repo stars](https://badgen.net/github/stars/CASIA-IVA-Lab/FastSAM)|Fast Segment Anything|-|
|[a16z-infra/ai-getting-started](https://github.com/a16z-infra/ai-getting-started)|![GitHub Repo stars](https://badgen.net/github/stars/a16z-infra/ai-getting-started)|A Javascript AI getting started stack for weekend projects, including image/text models, vector stores, auth, and deployment configs|-|
|[spacedriveapp/spacedrive](https://github.com/spacedriveapp/spacedrive)|![GitHub Repo stars](https://badgen.net/github/stars/spacedriveapp/spacedrive)|Spacedrive is an open source cross-platform file explorer, powered by a virtual distributed filesystem written in Rust.|-|
|[SkalskiP/top-cvpr-2023-papers](https://github.com/SkalskiP/top-cvpr-2023-papers)|![GitHub Repo stars](https://badgen.net/github/stars/SkalskiP/top-cvpr-2023-papers)|This repository is a curated collection of the most exciting and influential CVPR 2023 papers. 🔥 [Paper + Code]|-|
|[zksync/credo](https://github.com/zksync/credo)|![GitHub Repo stars](https://badgen.net/github/stars/zksync/credo)|No Description|-|
|[chat2db/Chat2DB](https://github.com/chat2db/Chat2DB)|![GitHub Repo stars](https://badgen.net/github/stars/chat2db/Chat2DB)|🔥 🔥 🔥 An intelligent and versatile general-purpose SQL client and reporting tool for databases which integrates ChatGPT capabilities.(智能的通用数据库SQL客户端和报表工具)|-|
|[embedchain/embedchain](https://github.com/embedchain/embedchain)|![GitHub Repo stars](https://badgen.net/github/stars/embedchain/embedchain)|Framework to easily create LLM powered bots over any dataset.|-|
|[papers-we-love/papers-we-love](https://github.com/papers-we-love/papers-we-love)|![GitHub Repo stars](https://badgen.net/github/stars/papers-we-love/papers-we-love)|Papers from the computer science community to read and discuss.|-|
|[ventoy/Ventoy](https://github.com/ventoy/Ventoy)|![GitHub Repo stars](https://badgen.net/github/stars/ventoy/Ventoy)|A new bootable USB solution.|-|
|[sindresorhus/awesome](https://github.com/sindresorhus/awesome)|![GitHub Repo stars](https://badgen.net/github/stars/sindresorhus/awesome)|😎 Awesome lists about all kinds of interesting topics|-|
|[ramonvc/freegpt-webui](https://github.com/ramonvc/freegpt-webui)|![GitHub Repo stars](https://badgen.net/github/stars/ramonvc/freegpt-webui)|GPT 3.5/4 with a Chat Web UI. No API key required.|-|
|[nayuki/QR-Code-generator](https://github.com/nayuki/QR-Code-generator)|![GitHub Repo stars](https://badgen.net/github/stars/nayuki/QR-Code-generator)|High-quality QR Code generator library in Java, TypeScript/JavaScript, Python, Rust, C++, C.|-|
|[devfullcycle/imersao13](https://github.com/devfullcycle/imersao13)|![GitHub Repo stars](https://badgen.net/github/stars/devfullcycle/imersao13)|No Description|-|
|[PlexPt/awesome-chatgpt-prompts-zh](https://github.com/PlexPt/awesome-chatgpt-prompts-zh)|![GitHub Repo stars](https://badgen.net/github/stars/PlexPt/awesome-chatgpt-prompts-zh)|ChatGPT 中文调教指南。各种场景使用指南。学习怎么让它听你的话。|-|
|[TodePond/DreamBerd](https://github.com/TodePond/DreamBerd)|![GitHub Repo stars](https://badgen.net/github/stars/TodePond/DreamBerd)|perfect programming language|-|
|[PCSX2/pcsx2](https://github.com/PCSX2/pcsx2)|![GitHub Repo stars](https://badgen.net/github/stars/PCSX2/pcsx2)|PCSX2 - The Playstation 2 Emulator|-|
|[s0md3v/sd-webui-roop](https://github.com/s0md3v/sd-webui-roop)|![GitHub Repo stars](https://badgen.net/github/stars/s0md3v/sd-webui-roop)|roop extension for StableDiffusion web-ui|-|
|[xtekky/gpt4free](https://github.com/xtekky/gpt4free)|![GitHub Repo stars](https://badgen.net/github/stars/xtekky/gpt4free)|The official gpt4free repository | various collection of powerful language models|-|
|[acidanthera/OpenCorePkg](https://github.com/acidanthera/OpenCorePkg)|![GitHub Repo stars](https://badgen.net/github/stars/acidanthera/OpenCorePkg)|OpenCore bootloader|-|
|[firstcontributions/first-contributions](https://github.com/firstcontributions/first-contributions)|![GitHub Repo stars](https://badgen.net/github/stars/firstcontributions/first-contributions)|🚀✨ Help beginners to contribute to open source projects|-|
|[Rapptz/discord.py](https://github.com/Rapptz/discord.py)|![GitHub Repo stars](https://badgen.net/github/stars/Rapptz/discord.py)|An API wrapper for Discord written in Python.|-|
|[OpenDriveLab/UniAD](https://github.com/OpenDriveLab/UniAD)|![GitHub Repo stars](https://badgen.net/github/stars/OpenDriveLab/UniAD)|[CVPR 2023 Best Paper] Planning-oriented Autonomous Driving|-|
